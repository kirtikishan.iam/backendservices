import { Bucket, StackContext, Table } from "sst/constructs";

export function StorageStack({ stack }: StackContext) {
    // Create the DynamoDB table
    const userTable = new Table(stack, "User", {
        fields: {
            userId: "string",
            uniqueId: "string",
            firstName: "string",
            lastName: "string",
            email: "string",
            phone: "number",
            userType: "string",
            createdAt: "string",
        },
        primaryIndex: { partitionKey: "uniqueId", sortKey: "email" },
    });

    const opportunityTable = new Table(stack, "Opportunity", {
      fields: {
        opportunityId: "string",
        userId: "string",   // each user here is mapped to above userId in "USER" table
        propertyId: "string",
        facilityId: "string",
        agentId: "string",
        createdAt: "string",
        stage: "number", // this is to save which stage customer has saved the application and left
      },
      primaryIndex: { partitionKey: "opportunityId", sortKey: "userId" },
    });

    const propertyTable = new Table(stack, "Property", {
      fields: {
        propertyId: "string",
        addressId: "string",
        ImageId: "string",
        bedRooms: "number",
        bathRooms: "number",
        parking: "number",
        landSize: "number",
        builtSize: "number",
        garageSpaces: "number",
        builtDate: "string",
        airCondition: "binary",
        onSuite: "binary",
        pool: "binary",
        wardrobe: "binary",
        study: "binary",
        pantry: "binary",
        basement: "binary",
        postCode: "number",
      },
      primaryIndex: { partitionKey: "propertyId", sortKey: "addressId" },
    });

    const addressTable = new Table(stack, "Address", {
      fields: {
        addressId: "string",
        state: "string",
        suburb: "string",
        address1: "string",
        address2: "string",
        houseNumber: "number",
        unitNumber: "number",
        latitude: "number",
        longitude: "number",
        postCode: "number",
      },
      primaryIndex: { partitionKey: "addressId" },
    });

    const facilityTable = new Table(stack, "Facility", {
      fields: {
        facilityId: "string",
        purpose: "string",
        value: "string",
        maxValue: "string",
        minValue: "string",
        saleValue: "string",
        inspectionType: "string", // Private/ Agent
        inspectionTimes: "string",
      },
      primaryIndex: { partitionKey: "facilityId" },
    });

    // Create an S3 bucket
    const propertyImagesBucket = new Bucket(stack, "PropertyImages", {
      cors: [
        {
          maxAge: "1 day",
          allowedOrigins: ["*"],
          allowedHeaders: ["*"],
          allowedMethods: ["GET", "PUT", "POST", "DELETE", "HEAD"],
        },
      ],
    });

    return {
        userTable,
        propertyImagesBucket,
        opportunityTable,
        propertyTable,
        addressTable,
        facilityTable,
    };
}