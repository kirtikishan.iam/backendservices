import { Api, StackContext, use } from "sst/constructs";
import { StorageStack } from "./StorageStack";

export function ApiStack({ stack }: StackContext) {
  const { userTable } = use(StorageStack);

  // Create the API
  const userAPI = new Api(stack, "Api", {
    defaults: {
      authorizer: "iam",
      function: {
        bind: [userTable],
      },
    },
    cors: {
      allowMethods: ["GET", "POST", "PUT"],
    },
    routes: {
      "POST /user": "packages/functions/src/user/createUser.main",
      "GET /userDetails/{id}": "packages/functions/src/user/getUserDetails.main",
      "GET /user/{id}": "packages/functions/src/user/getUser.main",
      "GET /users": "packages/functions/src/user/listUsers.main",
      "PUT /user/{id}": "packages/functions/src/user/updateUser.main",
    },
  });

  // Show the API endpoint in the output
  stack.addOutputs({
    ApiEndpoint: userAPI.url,
  });

  // Return the API resource
  return {
    userAPI,
  };
}