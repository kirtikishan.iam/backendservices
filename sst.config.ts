import { SSTConfig } from "sst";
import { StorageStack } from "./stacks/StorageStack";
import {ApiStack} from "./stacks/APIStack";
import { AuthStack } from "./stacks/AuthStack";

export default {
  config(_input) {
    return {
      name: "backendservices3",
      region: "ap-southeast-2",
    };
  },
  stacks(app) {
    app.stack(StorageStack).stack(ApiStack).stack(AuthStack);
  }
} satisfies SSTConfig;
