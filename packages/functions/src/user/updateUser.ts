import { Table } from "sst/node/table";
import handler from "@backendservices/core/handler";
import dynamoDb from "@backendservices/core/dynamodb";
import { generateUpdateQuery } from "@backendservices/core/helper";

export const main = handler(async (event) => {
    const data = JSON.parse(event.body || "{}");
    const expression = generateUpdateQuery(data);
    const params = {
        TableName: Table.User.tableName,
        Key: {
            uniqueId: event?.pathParameters?.id, // The uniqueId of the author
            email:  event?.queryStringParameters?.emailId, // The emailid of the user
        },
        ...expression,
        // 'ReturnValues' specifies if and how to return the item's attributes,
        // where ALL_NEW returns all attributes of the item after the update; you
        // can inspect 'result' below to see how it works with different settings
        ReturnValues: "ALL_NEW",
    };

    await dynamoDb.update(params);

    return JSON.stringify({ status: true });
});