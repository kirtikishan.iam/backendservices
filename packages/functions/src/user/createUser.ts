import { nanoid } from 'nanoid'
import handler from "@backendservices/core/handler";
import dynamoDb from "@backendservices/core/dynamodb";
import { Table } from "sst/node/table";


export const main = handler(async (event) => {
    let data = {
        userId: "",
        uniqueId: "",
        firstName: "",
        lastName: "",
        emailId: "",
        mobileNumber: "",
        userType: "",
        createdAt: "",
    };

    if (event.body != null) {
        data = JSON.parse(event.body);
    }

    const params = {
        TableName: Table.User.tableName,
        Item: {
            // The attributes of the item to be created
            userId: event.requestContext.authorizer?.iam.cognitoIdentity.identityId,
            uniqueId: nanoid(6),
            firstName: data.firstName,
            lastName: data.lastName,
            email: data.emailId,
            phone: data.mobileNumber,
            userType: "Customer",
            createdAt: Date.now(),
        },
    };

    const result = await dynamoDb.put(params);
    return JSON.stringify(params.Item);
});