import { Table } from "sst/node/table";
import handler from "@backendservices/core/handler";
import dynamoDb from "@backendservices/core/dynamodb";

export const main = handler(async (event) => {
    const params = {
        TableName: Table.User.tableName,
        // 'KeyConditionExpression' defines the condition for the query
        // - 'userId = :userId': only return items with matching 'userId'
        //   partition key
        KeyConditionExpression: "uniqueId = :uniqueId",
        // 'ExpressionAttributeValues' defines the value in the condition
        // - ':userId': defines 'userId' to be the id of the author
        ExpressionAttributeValues: {
            ":uniqueId": "umlxyO",
        },
    };

    const result = await dynamoDb.query(params);

    // Return the matching list of items in response body
    return JSON.stringify(result.Items);
});