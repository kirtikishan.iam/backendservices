import { Table } from "sst/node/table";
import handler from "@backendservices/core/handler";
import dynamoDb from "@backendservices/core/dynamodb";
import * as console from "console";

export const main = handler(async (event) => {
    const params = {
        TableName: Table.User.tableName,
        // 'Key' defines the partition key and sort key of
        // the item to be retrieved
        Key: {
            userId: event?.pathParameters?.id,
            email: event?.queryStringParameters?.emailId
        },
    };

    const result = await dynamoDb.get(params);
    if (!result.Item) {
        throw new Error("Item not found.");
    }

    // Return the retrieved item
    return JSON.stringify(result.Item);
});