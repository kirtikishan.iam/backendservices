export const generateUpdateQuery = (fields:any) => {
    let exp = {
        UpdateExpression: "set",
        ExpressionAttributeValues: {} as any,
    };
    Object.entries(fields).forEach(([key, item]) => {
        exp.UpdateExpression += ` ${key} = :${key},`;
        exp.ExpressionAttributeValues[`:${key}`] = item;
    });
    exp.UpdateExpression = exp.UpdateExpression.slice(0, -1);
    return exp;
};